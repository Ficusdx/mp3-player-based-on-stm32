#ifndef __KEY_H
#define __KEY_H
#include "stm32f10x.h"                  // Device header
#include "sys.h"

#define KEY0 (!(PEin(4)))
#define KEY1 (!(PEin(3)))
void key_init(void);


#endif
