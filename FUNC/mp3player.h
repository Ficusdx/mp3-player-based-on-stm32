#ifndef __MP3PLAYER_H
#define __MP3PLAYER_H
#include "stm32f10x.h"
#include "ff.h"

typedef enum mp3_ERROR
{
	MP3_OK,
	MP3_ERROR,
	MP3_FR_ERR,
	MP3_INVALID_FILE,
	MP3_CORRUPTION,
	MP3_F_CHUNK_ERR,
	MP3_UNSUPPORT,
	MP3_WAVE_NOTFOUND,
	MP3_END,
	
}MP3_RESULT;

typedef struct MICROSOFT_WAVE_COM
{
	char ID[4];
	uint32_t Size;
}MS_COM;

typedef struct MICROSOFT_WAVE_RIFF
{
	char ChunkID[4];
	u32 ChunkSize;
	char Format[4];
}MS_RIFF;

typedef struct MICROSOFT_WAVE_FMT
{
	char Subchunk1ID[4];
	uint32_t Subchunk1Size;
	uint16_t AudioFormat;
	uint16_t NumChannels;
	uint32_t SampleRate;
	uint32_t ByteRate;
	uint16_t BlockAlign;
	uint16_t BitsPerSample;
}MS_fmt;

typedef struct MICROSOFT_WAVE_DATA
{
	char SubchunkID[4];
	uint32_t SubchunkSize;
}MS_data;

typedef struct{
	uint32_t freq;
	uint16_t channels;
	uint32_t bit_rate;
	uint32_t f_size;
}mp3_info;

#define MP3_HANDLE	{mp3_err_handle(); printf("%s,%d\r\n",__FILE__,__LINE__);}
#define BUFF_SIZE	 (16*1024) //16kB���뻺����
#define PCM_SIZE	 (1152*2)//PCM������
#define PALY_BUFSIZE 2048 //byte
#define ROOT_DIR_PATH   "0:/"

MP3_RESULT mp3_playwave(FIL* file);//dec wav file, get the audio info and play it
MP3_RESULT mp3_decmp3into_wave(FIL* f_mp3, const char* mp3_n);//dec mp3 into wav, and create a file at memory disk
MP3_RESULT mp3_findwave(DIR dir, char* wavname, const char * fname);
MP3_RESULT mp3_fill_buf(FIL* file);
MP3_RESULT mp3_create_wavchunk(FIL* f_wav, mp3_info* info);
void player_Init(u32 Sample, u32 channel);
char* ext_fname(char* fn);
char* first_fname(char* ffname, const char* fn);


#endif
