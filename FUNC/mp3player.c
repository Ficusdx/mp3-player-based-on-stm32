#define MINIMP3_IMPLEMENTATION
#include "stm32f10x.h"                  // Device header
#include "minimp3.h"
#include "mp3player.h"
#include "sdcard.h"
#include "SoundDriver.h"
#include "usart.h"
#include "key.h"
#define MIN_REMAIN (3*1024)//

//��Դ����֧�֣�minimp3��Author : lieff

mp3dec_frame_info_t info;

__align(4) u8 mp3_buf[BUFF_SIZE];//16kB
u8* phead = mp3_buf;//the head of mp3_buf
u32 buf_remain;//mp3_buf remain nums
__align(4) mp3d_sample_t pcm_buf[PCM_SIZE];//pcm_buf

MP3_RESULT mp3_findwave(DIR dir, char* wavname, const char * fname)
{
	FRESULT fr;
	FILINFO finfo = {0};
	
	first_fname(wavname, fname);
	strncat(wavname, ".WAV", 5);//the last is '\0'
	 
	f_rewinddir(&dir);//rewind the dir to look for
	
	do{
		fr = f_readdir(&dir, &finfo);
		if(fr != FR_OK)return MP3_FR_ERR;
		if(finfo.fname[0] == '\0')
			return MP3_WAVE_NOTFOUND;
	}while(strcmp(wavname, finfo.fname));
	
	//found wav file with the same name
	return MP3_OK;
}

void mp3_err_handle(void)
{
	TIM_Cmd(TIM2, DISABLE);
	DMA_Cmd(DMA2_Channel3, DISABLE);
	DAC_Cmd(DAC_Channel_1, DISABLE);
	DAC_DMACmd(DAC_Channel_1, DISABLE);
	
	while(1);
}

static MP3_RESULT wav_find_chunk(uint8_t* buf, FIL* file , const char* chunkID)
{
	FRESULT fr;
	UINT br;
	int time = 20;
	do{
		fr = f_read(file, buf, sizeof(MS_COM), &br);
		if(fr != FR_OK)return MP3_FR_ERR;
		if(br < sizeof(MS_COM))
			return MP3_CORRUPTION;

		if(!memcmp(((MS_COM*)buf)->ID, chunkID, 4))
		{//compatible ID
			if(memcmp(((MS_COM*)buf)->ID, "data", 4)){
				//not data ID
				fr = f_read(file, buf+sizeof(MS_COM), ((MS_COM*)buf)->Size, &br);
				if(fr != FR_OK)					return MP3_FR_ERR;
				if(br < sizeof(MS_COM))	return MP3_CORRUPTION;
			}
			return MP3_OK;
		}
		else{
			if(memcmp(((MS_COM*)buf)->ID, "data", 4))//not data ID
				f_lseek(file, file->fptr + ((MS_COM*)buf)->Size);
			else
				//its data ID
				return MP3_CORRUPTION;
		}
	}while(time-- > 0);
	return MP3_CORRUPTION;
}

MP3_RESULT mp3_playwave(FIL* file)
{
	TIM_DeInit(TIM2);
	DAC_DeInit();
	
	//analyse wave file
	__align(4) uint8_t buf[64];
	UINT br,flag = 0;
	FRESULT fr;
	MP3_RESULT mr;
	void* ms_struct;
	uint32_t sample_rate;
	uint16_t num_channels;
	
	fr = f_read(file, buf, sizeof(MS_RIFF), &br);//get RIFF struction
	if(fr != FR_OK)return MP3_FR_ERR;
	if(br < sizeof(MS_RIFF))
		return MP3_CORRUPTION;
	ms_struct = buf;
	if(memcmp(((MS_RIFF*)ms_struct)->ChunkID, "RIFF",4))//return if this not a wav file
		return MP3_INVALID_FILE;
	
	mr = wav_find_chunk(buf, file, "fmt ");//find chunk fmt
	if(mr != MP3_OK)return mr;
	num_channels = ((MS_fmt*)buf)->NumChannels;//get channles num
	sample_rate = ((MS_fmt*)buf)->SampleRate;//get sample rate
	if(((MS_fmt*)buf)->AudioFormat != 1)return MP3_UNSUPPORT;
	
	mr = wav_find_chunk(buf, file, "data");//find chunk :data
	if(mr != MP3_OK)return mr;
	
	player_Init(sample_rate, num_channels);
	
	DMA_Cmd(DMA2_Channel3, ENABLE);
	DAC_Cmd(DAC_Channel_1, ENABLE);
	DAC_DMACmd(DAC_Channel_1, ENABLE);
	TIM_ClearFlag(TIM12, TIM_FLAG_Update);
	TIM_Cmd(TIM2, ENABLE);
	
	do
	{
		if(KEY0){
			DMA2->IFCR |= 0x10;
			DMA2->IFCR &= 0xFFFFFFEF;
			TIM_Cmd(TIM2, DISABLE);
			if(fr != FR_OK)return MP3_FR_ERR;
			break;
		}
	
		if(DMA_GetFlagStatus(DMA2_FLAG_HT3))		//over the half
		{
			memset(mp3_buf, 0, 1024);
			
			if(f_eof(file))
			{
				memset(mp3_buf + (flag++<<10), 0, 1024);
				DMA_ClearFlag(DMA2_FLAG_HT3);
				continue;
			}
			fr = f_read(file, mp3_buf, 1024, &br);
			if(fr != FR_OK){printf("FR_ERROR\r\n");TIM_Cmd(TIM2, DISABLE); return MP3_FR_ERR;}
			
			for(int i = 0;i<0x100;i++)//one channle only -handle
				*((uint32_t*)mp3_buf + i) ^= 0x8000;
			
			DMA_ClearFlag(DMA2_FLAG_HT3);
		}
		if(DMA_GetFlagStatus(DMA2_FLAG_TC3))   //over the end
		{
			memset(mp3_buf + 1024, 0, 1024);
			if(f_eof(file))
			{
				flag++;
				DMA_ClearFlag(DMA2_FLAG_TC3);
				continue;
			}
			
			fr = f_read(file, mp3_buf + (PALY_BUFSIZE>>1), 1024, &br);
			if(fr != FR_OK){printf("FR_ERROR\r\n");TIM_Cmd(TIM2, DISABLE); return MP3_FR_ERR;}
		
			for(int i = 0;i<0x100;i++)//one channle only -handle
				*((uint32_t*)mp3_buf+ 0x100 + i) ^= 0x8000;
			
			DMA_ClearFlag(DMA2_FLAG_TC3);
		}
	}while(flag < 2);
	DMA2->IFCR |= 0x10;
	DMA2->IFCR &= 0xFFFFFFEF;
	printf("playing complete\r\n");
	TIM_Cmd(TIM2, DISABLE);
	return MP3_END;
}

MP3_RESULT mp3_decmp3into_wave(FIL* f_mp3, const char* wav_n)
{
	printf("decoding\r\n");
	buf_remain = 0;
	FIL f_wav;
	FRESULT fr;
	MP3_RESULT mr;
	UINT bw, decode_n = 1, br;
	uint32_t samples = 0;
	mp3dec_t mp3d;
	mp3_info m_inf;
	char path[20] = ROOT_DIR_PATH;
	
	mp3dec_init(&mp3d);
	
	fr = f_open(&f_wav, strcat(path, wav_n), FA_CREATE_ALWAYS | FA_WRITE);
	if(fr != FR_OK)return MP3_FR_ERR;
	
	fr = f_lseek(&f_wav, sizeof(MS_RIFF)+sizeof(MS_fmt)+sizeof(MS_data));//skip wav chunk 
	if(fr != FR_OK)return MP3_FR_ERR;
	
	//------------------------------------------------------------------------------
	//decode main programa
	do
    {
        samples = mp3dec_decode_frame(&mp3d, phead, buf_remain, (mp3d_sample_t*)pcm_buf, &info);
        fr = f_write(&f_wav, pcm_buf, info.channels * sizeof(short) * samples, &bw);
				if(fr != FR_OK)return MP3_FR_ERR;
			
        printf("decoded samples = %d X %d\r\n",samples, decode_n++);
        if (samples != 0)//dec success
        {
            phead += info.frame_bytes;
            buf_remain -= info.frame_bytes;
        }else{//no success
            if (f_eof(f_mp3))//is mp3 end? yes
            {
                break;//complete
            }else{//mp3 not end
                buf_remain = MIN_REMAIN;
                phead = mp3_buf + BUFF_SIZE - buf_remain;
            }
        }
        memmove(mp3_buf, phead, buf_remain);
        fr = f_read(f_mp3, mp3_buf + buf_remain, sizeof(char) * (BUFF_SIZE - buf_remain), &br);
				if(fr != FR_OK)return MP3_FR_ERR;
				buf_remain += br;
        phead = mp3_buf; 
    } while (1);
	//---------------------------------------------------------------------------------

	m_inf.freq = info.hz;
	m_inf.channels = info.channels;
	m_inf.bit_rate = info.bitrate_kbps;
	m_inf.f_size = f_wav.fptr;
		
	mr = mp3_create_wavchunk(&f_wav, &m_inf);
	if(mr != MP3_OK)return mr;
	
	fr = f_close(&f_wav);
	if(fr != FR_OK)return MP3_FR_ERR;
	
	return MP3_OK;
}

////���MP3buff��������λ��
//MP3_RESULT mp3_fill_buf(FIL* file)
//{
//	FRESULT fr;
//	UINT br;
//	
//	memmove(mp3_buf, phead_buf, buf_remain);
//	phead_buf = mp3_buf;
//	
//	fr = f_read(file, mp3_buf + buf_remain, BUF_SIZE - buf_remain, &br);
//	if(fr != FR_OK)return MP3_FR_ERR;
//	
//	buf_remain += br;
//	return MP3_OK;
//}

MP3_RESULT mp3_create_wavchunk(FIL* f_wav, mp3_info* info)
{
	FRESULT fr;
	UINT bw;
	MS_data ms_data = {{'d','a','t','a'},info->f_size - sizeof(MS_RIFF) - sizeof(MS_fmt) - 8};
	MS_fmt ms_fmt = {{'f','m','t',' '}};
	MS_RIFF ms_riff = {{'R','I','F','F'},info->f_size - 8,{'W','A','V','E'}};
	
	f_lseek(f_wav, 0);//move to head
	
	fr = f_write(f_wav, &ms_riff, sizeof(ms_riff), &bw);
	if(fr != FR_OK || bw<sizeof(ms_riff))return MP3_FR_ERR;
	
	ms_fmt.AudioFormat = 1;
	ms_fmt.BitsPerSample = 16;
	ms_fmt.BlockAlign = info->channels*16/8;
	ms_fmt.ByteRate = info->freq*info->channels*16/8;//SampleRateNumChannelsBitsPerSample/8
	ms_fmt.NumChannels = info->channels;
	ms_fmt.SampleRate = info->freq;
	ms_fmt.Subchunk1Size = 16;
	fr = f_write(f_wav, &ms_fmt, sizeof(ms_fmt), &bw);
	if(fr != FR_OK || bw<sizeof(ms_fmt))return MP3_FR_ERR;
	
	fr = f_write(f_wav, &ms_data, sizeof(ms_data), &bw);
	if(fr != FR_OK || bw<sizeof(ms_data))return MP3_FR_ERR;
	return MP3_OK;
}
	
//mp3_ERROR mp3_play(fat32_file* file)
//{
//	int samples = 0;
//	static mp3dec_t mp3d;
//	mp3dec_init(&mp3d);
//	buf_remain = 0;
//	fat32_file pcm_file;
//	u32 FAT_Buf[128];
//	
//	fat32_file_init(&pcm_file);//��ʼ��pcm�ļ�
//	pcm_file.Volume = file->Volume;
//	if(mp3_End == fill_mp3_buf(file))
//	{
//		return mp3_End;
//	}
//	do{
//		samples = mp3dec_decode_frame(&mp3d, phead_buf, buf_remain, pcm, &info);
//		buf_remain -= info.frame_bytes;
//		phead_buf += info.frame_bytes;
//		if(buf_remain<MINIMUM_BUF_REMAIN)
//			fill_mp3_buf(file);
//	}while(samples == 0);
//	for(int i = 0;i<(MINIMP3_MAX_SAMPLES_PER_FRAME);i++)
//		pcm[i] ^= 0x8000;
//	while(FileFound == fat32_findExt(&pcm_file, "PCM"))
//	{
//		if(!memcmp(file->fname, pcm_file.fname, 8))
//			//�����ͬ���ļ�
//		{
//			pcm_play(&pcm_file, info.channels, info.hz);//ֱ�Ӳ���
//			return mp3_playing;
//		}
//	}
//	//û��ͬ��pcm�ļ�
//	//��ʼ����mp3������pcm�ļ�
//	fat32_fnew(&pcm_file, file->fname, file->Volume, FAT_Buf);//fnewʹ��SD_Buffer��Ŀ¼��
//	u32 pcm_occupied;//���뻺����
//	u32 pcm_buf_occupied;//д�뻺������
//	u16 pcm_buf[256];//д�뻺����
//	pcm_buf_occupied = 0;
//	do{
//		pcm_occupied = MINIMP3_MAX_SAMPLES_PER_FRAME;
//		for(;pcm_occupied > 0;pcm_buf_occupied++,pcm_occupied--)
//		{
//			if(pcm_buf_occupied >= 256)
//			{//pcmд�뻺������	
//				printf("s %d\r\n", fat32_fwrite(&pcm_file, pcm_buf, FAT_Buf));
//				pcm_buf_occupied = 0;
//				memset(pcm_buf, 0, 512);
//			}
//			pcm_buf[pcm_buf_occupied] = pcm[MINIMP3_MAX_SAMPLES_PER_FRAME - pcm_occupied];
//		}
//	}while(mp3_End != mp3_decode(pcm,file));
//	if(pcm_buf_occupied > 0)
//		fat32_fwrite(&pcm_file, pcm_buf, FAT_Buf);
//	fat32_fclose(&pcm_file, FAT_Buf);
//	return mp3_OK;
//}

////mp3����һ֡�������Զ�ȡ�ļ�
//mp3_ERROR mp3_decode(mp3d_sample_t* pcm, fat32_file* file)
//{
//	static mp3dec_t mp3d;
//	static mp3_ERROR file_flag = mp3_OK;
//	int samples = 0;
//  do{
//		samples = mp3dec_decode_frame(&mp3d, phead_buf, buf_remain, pcm, &info);
//		buf_remain -= info.frame_bytes;
//		phead_buf += info.frame_bytes;
//		if(buf_remain<MINIMUM_BUF_REMAIN)
//			if(mp3_End != file_flag)
//			{
//				file_flag = fill_mp3_buf(file);
//			}
//			if(samples == 0 && file_flag == mp3_End)
//				return mp3_End;
//	}while(samples == 0);
//	for(int i = 0;i<(MINIMP3_MAX_SAMPLES_PER_FRAME);i++)
//		pcm[i] ^= 0x8000;
//	return mp3_OK;
//}

void player_Init(u32 Sample, u32 channel)
{
	TIM_Cmd(TIM2, DISABLE);
	TIM_DeInit(TIM2);
	DMA_DeInit(DMA2_Channel3);
	DAC_DeInit();
	
	
	switch(Sample)
	{
		case 44100:
			Sample = Sample44100;
			break;
		case 22050:
			Sample = Sample22050;
			break;
		case 48000:
			Sample = Sample48000;
			break;
	}
	
	//enable clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC | RCC_APB1Periph_TIM2, ENABLE);//DACʱ��
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);//GPIOA,ʱ��
	
	//��ʱ����ʼ��
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_TimeBaseInitStruct.TIM_Period = Sample;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 0x01;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct);
	
	TIM2->CR2 = (u16)0x20;
	//DMA��ʼ��
	DMA_InitTypeDef DMAInitStruct;
	
	//DMAʱ��ʹ��
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
	
	//DAC1ͨ��������DMA����
	DMAInitStruct.DMA_BufferSize = PALY_BUFSIZE>>(channel);
	DMAInitStruct.DMA_DIR = DMA_DIR_PeripheralDST;
	DMAInitStruct.DMA_M2M = DMA_M2M_Disable;
	DMAInitStruct.DMA_MemoryBaseAddr = (u32)mp3_buf;
	DMAInitStruct.DMA_MemoryDataSize = 0x200<<(channel);
	DMAInitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMAInitStruct.DMA_Mode = DMA_Mode_Circular;
	DMAInitStruct.DMA_PeripheralBaseAddr = (u32)(&DAC->DHR12L1);//�����12λͨ��1
	DMAInitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMAInitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMAInitStruct.DMA_Priority = DMA_Priority_Medium;
	DMA_Init(DMA2_Channel3, &DMAInitStruct);

	//�ж���������
//	NVIC_InitStruct.NVIC_IRQChannel = DMA2_Channel3_IRQn;
//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
//	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
//	NVIC_Init( &NVIC_InitStruct);
	
	//DMA_ITConfig(DMA2_Channel3, DMA_IT_HT | DMA_IT_TC, ENABLE);
	
	DAC_InitTypeDef DACInitStruct;//DAC��ʼ��
	GPIO_InitTypeDef GPIOInitStruct;//GPIO��ʼ��
	
	//GPIOA  PA4����Ϊģ�������ų����ţ�ʹ��DAC��PA4�Զ�����ģ�����
	GPIOInitStruct.GPIO_Mode = GPIO_Mode_AIN;
	GPIOInitStruct.GPIO_Pin = GPIO_Pin_4;
	GPIOInitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIOInitStruct);
	
	//DAC1	������壬T2��ʱ���������޲���
	DACInitStruct.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
	DACInitStruct.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
	DACInitStruct.DAC_Trigger = DAC_Trigger_T2_TRGO;
	DACInitStruct.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_Init(DAC_Channel_1,&DACInitStruct);
	
	DAC_SetChannel1Data(DAC_Align_12b_L, 0x0000);
}

//find a file's extension
char* ext_fname(char* fn){
	char* exn;
	int slen = 0;
	slen = strlen(fn);
	while(fn[slen] != '.' && slen-- > 0);
	exn = fn + slen + 1;
	return exn;
}

//copy a file's first name
char* first_fname(char* ffname, const char* fn)
{
	int i = 0;
	for(;fn[i] != '.' && fn[i] != 0;i++);
	memcpy(ffname, fn, i);
	ffname[i] = '\0';
	return ffname;
}
