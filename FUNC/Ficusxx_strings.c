#include <stdio.h>
#include <string.h>
#include "Ficusxx_strings.h"
#define END (0x64)
#define limit 4095
/*======================================================
* @ brief :	将BCD码串转换为字符串，BCD格式：123 -> 321<END>
* @ example:	1234<END> -> "1234"
* @ parama:	X -> 待转换BCD码串的指针
* @ retval: X -> 处理后返回原指针
=======================================================*/
char* BCD2str(char* X)
{
	for (int i = 0; i < limit; i++)
	{
		if (X[i] == END)
		{
			X[i] = 0;
			return X;
		}
		else
		{
			X[i] += '0';
		}
	}
	return X;
}
/*======================================================
* @ brief :	将字符串转换为BCD码串，BCD格式：123 -> "321<END>"
* @ example:	"1234" -> 1234<END>
* @ parama:	X -> 待转换的串X
* @ retval:	X -> 处理后返回原指针
======================================================*/
char* str2BCD(char* X)
{
	for (int i = 0; i < limit; i++)
	{
		if (X[i] == '\0')
		{
			X[i] = END;
			return X;
		}
		else
		{
			X[i] -= '0';
		}
	}
	return X;
}
/*======================================================
	@ brief: 整形变为倒序的字符串
	@ example: 123 -> "321"
	@ parama: n -> 待转换的整数
	@ retval: X -> 处理后返回字符串指针
=======================================================*/
char* int2Rstr(char* X,int n)
{
	int i = 0;
	while (n)
	{
		X[i++] = '0' + n % 10;
		n /= 10;
	}
	X[i] = '\0';
	return X;
}
/*======================================================
	@ brief: ascii字符串倒置
	@ example: "abcd123" -> "321dcba"
	@ parama: char* X -> 待处理串的指针
	@ retval: char* X -> 处理后返回原指针
=======================================================*/
char* reverse_str(char* X)
{
	unsigned char count = 0;
	char tmp;
	char* p = X;
	while (p[++count] != '\0');
	count--;
	while (p - X < count)
	{
		tmp = X[count];
		X[count--] = *p;
		*p++ = tmp;
	}
	return X;
}

/*======================================================
	@ brief :	以小端字符串的形式进行乘法运算，串X空间必须大于等于
	串X长度+串Y长度。
		CAUTION: 小端排序
	@ example：	("8888","88") -> "441287"
	@ parama:	Z -> 目标存储区域
				X -> 被加串的指针
				Y -> 加数串的指针
				len -> 处理的最大长度，Z的最大长度
	@ retval:	X -> 处理后返回被加串指针
=======================================================*/
char* strMul(char* Z, char* X, char* Y, const int len)
{
	int X_len = 0;
	int Y_len = 0;
	int tmp = 0;
	char flagX = 1, flagY = 1;
	//清零Z
	for (int i = 0; i < len; i++)
	{
		Z[i] = 0;
	}
	//计算X串和Y串的长度
	do
	{
		if (X[tmp]=='\0' && flagX)
		{
			X_len = tmp;
			flagX = !flagX;
		}
		if (Y[tmp]=='\0' && flagY)
		{
			Y_len = tmp;
			flagY = !flagY;
		}
		tmp++;
	} while (X[X_len] || Y[Y_len]);
	/*printf("X = %d,Y = %d\n", X_len, Y_len);*/
	//转为BCD编码
	str2BCD(X);
	str2BCD(Y);
	Z[len - 1] = END;
	for(int j = 0;j < Y_len;j++)
	{
		for (int i = 0; i < X_len; i++)
		{
			Z[len - 2 - i - j] += X[X_len - 1 - i] * Y[Y_len - 1 - j];
			Z[len - 3 - i - j] += Z[len - 2 - i - j] / 10;
			Z[len - 2 - i - j] %= 10;
		}
	}
	if (Z[len - 1 - X_len - Y_len] == 0)
	{
		memmove(Z, Z + len - X_len - Y_len, X_len + Y_len);
	}
	else
	{
		memmove(Z, Z + len - X_len - Y_len - 1, X_len + Y_len + 1);
	}
	BCD2str(X);
	BCD2str(Y);
	return BCD2str(Z);
}

/*======================================================
	@ brief :	进行(小端)字符串加法，串X必须大于串Y，
		CAUTION: 小端排序
	@ example：	"321","4321" -> "7531"
	@ parama:	X -> 被加串的指针
				Y -> 加数串的指针
				len -> 处理的最大长度，X的最大长度
	@ retval:	X -> 处理后返回被加串指针
=======================================================*/
//char* RstrAdd(char* X, char* Y,const int len)
//{
//	 char* px = str2BCD(X);
//	const unsigned char* py = (const unsigned char*)str2BCD(Y);//转为BCD编码
//	unsigned char flag = 0;//进位标记
//	do
//	{
//		if (*px == END)
//			*px = 0;
//		if (*py != END )
//			*px += *py++;
//		else if (!flag)break;//串py运算结束
//		//进位标记运算
//		if (flag) 
//		{
//			*px += 1;
//			flag = 0;
//		}
//		//进位标记打开
//		if (*px > 9) 
//		{
//			*px -= 10;
//			flag = 1;
//		}
//		px++;
//	} while (px - X < len);
//	*px = END;
//	BCD2str(X);
//	BCD2str(Y);
//	return X;
//}


/*======================================================
* @ brief :	将两个整形相加并转化为字符串，保存至串X中。
* @ example:(1234,123,X,64) -> "1357"
* @ parama:	Var1 -> 被加数
*			Var2 -> 加数
*			X -> 于之保存的目标串X
*			len -> 串的最大长度		
* @ retval:	char* X -> 处理后返回原指针
======================================================*/


