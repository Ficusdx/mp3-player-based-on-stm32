#include <string.h>
#include "stm32f10x.h"
#include "usart.h"
#include "key.h"
#include "delay.h"
#include "SoundDriver.h"
#include "mp3player.h"
#include "ff.h"



int main()
{
	FATFS fatfs;
	DIR dir;
	FIL file;
	FILINFO finfo;
	FRESULT fr;
	MP3_RESULT mr;
	char path[30];
	
	delay_init();
	//NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	uart_init(14400);
	key_init();
	
	
	strcpy(path, ROOT_DIR_PATH);
	fr = f_mount(&fatfs, path, 0);//mount the file system from the SD
	printf("f_mount = %d\r\n",fr);
	fr = f_opendir(&dir,path);//open a dir
	printf("f_opendir = %d\r\n",fr);
	
	while(1){
		delay_ms(10);
		
		//read directory
		if(KEY0){
			do{
				fr = f_readdir(&dir, &finfo);
				if(fr != FR_OK)return fr;
				if(*finfo.fname == '\0'){
					f_rewinddir(&dir);//break when read over all the item
					break;
				}
			}while(strcmp(ext_fname(finfo.fname), "MP3") && strcmp(ext_fname(finfo.fname), "WAV"));
			printf("%s\r\n",finfo.fname);
			delay_ms(500);
		}
		
		//play this file
		if(KEY1){
			
			if(!strcmp(ext_fname(finfo.fname),"WAV")){
				fr = f_open(&file, strcat(path, finfo.fname), FA_READ);				
				if(fr != FR_OK)return fr;

				mp3_playwave(&file);
				fr = f_close(&file);
				
				strcpy(path, ROOT_DIR_PATH);//@0x0800472C
				delay_ms(500);
			}
			else if(!strcmp(ext_fname(finfo.fname),"MP3")){
				
				char found_name[13];
				mr = mp3_findwave(dir, found_name, finfo.fname);
				
				if(MP3_OK == mr){//found wave file with the same name
					fr = f_open(&file, strcat(path, found_name), FA_READ);
					if(fr != FR_OK)return fr;
					
					mp3_playwave(&file);
					fr = f_close(&file);
					strcpy(path, ROOT_DIR_PATH);
					delay_ms(500);
				}
				else if(MP3_WAVE_NOTFOUND == mr){
					
					//decode start
					//open this file
					fr = f_open(&file, strcat(path, finfo.fname), FA_READ);
					if(fr != FR_OK)return fr;//if f_open fail
					
					//DO NOT remove tf-card when it decoding
					//there is no protection for data writing
					mr = mp3_decmp3into_wave(&file, found_name);
					if(mr != MP3_OK){
						printf("decoding failure\r\n");
						strcpy(path, ROOT_DIR_PATH);
						f_unlink(strcat(path, found_name));
						continue;
					}
					
					f_close(&file);
					strcpy(path, ROOT_DIR_PATH);
					printf("decode success!!\r\n");
					delay_ms(500);
				}
				else 
					return mr;
			}
		}
	}
}
