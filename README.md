
### 开放源代码支持：

* > minimp3 -- lieff
* > fatfs   -- (C)ChaN, 2015

![输入图片说明](screenshot20230917.png)

经过测试，minimp3解码器运行时需要25KB的栈空间，因此需要对  *.s 的启动文件进行设置。
详情见CORE文件夹内的 *.s 文件。设置一定的裕量，使栈空间为 0x6800 byte。


